//
//  AVSnapHandler.swift
//  EmotionPhoto
//
//  Created by mauricio.marques on 26/04/18.
//  Copyright © 2018 mauricio.marques. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

final class AVSnapHandler: NSObject {
    fileprivate let outputHandler: (CIImage?) -> ()
    fileprivate var captureSession: AVCaptureSession?
    
    init(outputHandler: @escaping (CIImage?) -> ()) {
        self.outputHandler = outputHandler
    }
}

extension AVSnapHandler {
    
    func setupCaptureSession(forView view: UIView) {
        let captureSession = AVCaptureSession()
        
        let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType:AVMediaType.video, position: .front).devices
        
        do {
            if let captureDevice = availableDevices.first {
                captureSession.addInput(try AVCaptureDeviceInput(device: captureDevice))
            }
        } catch {
            print(error.localizedDescription)
        }
        
        let captureOutput = AVCaptureVideoDataOutput()
        captureSession.addOutput(captureOutput)
        
        captureOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue"))
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.frame
        view.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
        self.captureSession = captureSession
    }
    
    func stopCaptureSession() {
        self.captureSession?.stopRunning()
    }
    
    func resumeCaptureSession() {
        self.captureSession?.startRunning()
    }
}

extension AVSnapHandler: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        let pixelBuffer: CVPixelBuffer? = sampleBuffer.imageBuffer
        let ciImage = CIImage(cvImageBuffer: pixelBuffer!)
        
        //leftMirrored for front camera
        let ciImageWithOrientation = ciImage.oriented(forExifOrientation: Int32(UIImage.Orientation.leftMirrored.rawValue))
        
        self.outputHandler(ciImageWithOrientation)
    }
    
}
