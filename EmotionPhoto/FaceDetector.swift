//
//  FaceDetector.swift
//  EmotionPhoto
//
//  Created by Maurício Marques on 5/13/18.
//  Copyright © 2018 mauricio.marques. All rights reserved.
//

import Foundation
import Vision

enum FaceDetectionResult {
    case success(CGImage)
    case error
}

class FaceDetector {
    
    let startDetectionAction: () -> ()
    let faceDetectionAction: (FaceDetectionResult) -> ()
    let finishDetectionAction: (CGImage, Int) -> ()
    
    private var currentInterval: Int = 0
    private let maxInterval: Int = 10
    
    init(startDetectionAction: @escaping () -> (), faceDetectionAction: @escaping (FaceDetectionResult) -> (), finishDetectionAction: @escaping (CGImage, Int) -> ()) {
        self.startDetectionAction = startDetectionAction
        self.faceDetectionAction = faceDetectionAction
        self.finishDetectionAction = finishDetectionAction
    }
    
    func detectFaceOn(_ ciImage: CIImage) {
        let faceDetectionRequest = VNDetectFaceRectanglesRequest { (request, error) in
            guard let observations = request.results as? [VNFaceObservation], observations.count > 0 else {
                self.faceDetectionAction(.error)
                return
            }
            
            self.startDetectionAction()
            
            guard let cgImage = FaceDetector.convertCIImageToCGImage(ciImage) else { return }
            
            for face in observations {
                let width = face.boundingBox.width * CGFloat(cgImage.width)
                let height = face.boundingBox.height * CGFloat(cgImage.height)
                let x = face.boundingBox.origin.x * CGFloat(cgImage.width)
                let y = (1 - face.boundingBox.origin.y) * CGFloat(cgImage.height) - height
                
                let croppingRect = CGRect(x: x, y: y, width: width, height: height)
                if let faceImage = cgImage.cropping(to: croppingRect) {
                    self.faceDetectionAction(.success(faceImage))
                } else {
                    self.faceDetectionAction(.error)
                }
            }
            
            self.finishDetectionAction(cgImage, observations.count)            
        }
        
        if self.currentInterval == self.maxInterval {
            try? VNImageRequestHandler(ciImage: ciImage, options: [:]).perform([faceDetectionRequest])
            self.currentInterval = 0
        } else {
            self.currentInterval += 1
        }
    }
    
    static func convertCIImageToCGImage(_ inputImage: CIImage) -> CGImage? {
        let context = CIContext(options: nil)
        return context.createCGImage(inputImage, from: inputImage.extent)
    }
}
