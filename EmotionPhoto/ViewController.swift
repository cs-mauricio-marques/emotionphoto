//
//  ViewController.swift
//  EmotionPhoto
//
//  Created by mauricio.marques on 26/04/18.
//  Copyright © 2018 mauricio.marques. All rights reserved.
//

import UIKit
import Vision
import VideoToolbox

class ViewController: UIViewController {

    private var snapHandler: AVSnapHandler?
    private var faceDetector: FaceDetector?
    private let emotionClassifier = EmotionClassifier()
    private var picturesTaken = 0
    private var hasHappyFacesOnLoop = false
    private var isTakingPicture = false
    private var pictures = [UIImage]()
    private var facesAndEmotions = [CGImage: Emotion]()
    
    private lazy var doneButton = { () -> UIButton in
        let bt = UIButton(type: .system)
        bt.setTitle("OK", for: UIControl.State.normal)
        bt.setTitleColor(.gray, for: UIControl.State.normal)
        bt.addTarget(self, action: #selector(didTapDoneButton), for: UIControl.Event.touchUpInside)
        bt.backgroundColor = UIColor.white.withAlphaComponent(0.7)
        return bt
    }()
    
    private lazy var emoticonLabel = { () -> UILabel in
        let lb = UILabel()
        lb.textAlignment = .center
        return lb
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSnap()
        self.setupFaceDetector()
        self.setupDoneButton()
        self.setupEmoticonLabel()
    }

}

extension ViewController {
    
    private func setupSnap() {
        self.snapHandler = AVSnapHandler(outputHandler: self.snapOutputHandler)
        self.snapHandler?.setupCaptureSession(forView: self.view)
    }
    
    private func setupFaceDetector() {
        self.faceDetector = FaceDetector(startDetectionAction: self.startedDetectingFaces, faceDetectionAction: self.detectFaceWithResult, finishDetectionAction: self.finishedDetectingFaces)
    }
    
    private func setupDoneButton() {
        self.view.addSubview(self.doneButton)
        self.doneButton.translatesAutoresizingMaskIntoConstraints = false
        self.doneButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -32).isActive = true
        self.doneButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -32).isActive = true
        self.doneButton.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        self.doneButton.widthAnchor.constraint(equalToConstant: 50.0).isActive = true
        
        self.doneButton.layer.cornerRadius = 50.0 / 2
        self.doneButton.clipsToBounds = true
    }
    
    private func setupEmoticonLabel() {
        self.view.addSubview(self.emoticonLabel)
        self.emoticonLabel.translatesAutoresizingMaskIntoConstraints = false
        self.emoticonLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.emoticonLabel.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 200).isActive = true
        self.emoticonLabel.widthAnchor.constraint(equalToConstant: 100.0).isActive = true
        self.emoticonLabel.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
    }
    
}

extension ViewController {
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.snapHandler?.stopCaptureSession()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.snapHandler?.resumeCaptureSession()
    }
    
}

extension ViewController {
    
    private func startedDetectingFaces() {
        self.hasHappyFacesOnLoop = true
        self.facesAndEmotions.removeAll()
    }
    
    private func detectFaceWithResult(_ result: FaceDetectionResult) {
        switch result {
        case .success(let faceImage):
            self.classifyImage(faceImage)
        case .error:
            self.hasHappyFacesOnLoop = false
        }
    }
    
    private func finishedDetectingFaces(cgImage: CGImage, observations: Int) {
        if self.hasHappyFacesOnLoop {
            DispatchQueue.main.async {
                self.picturesTaken += 1
                let observationString = observations > 1 ? "\(observations) people" : "1 person"
                print("take picture n-\(self.picturesTaken) with \(observationString)")
                self.takePicture(cgImage)
                self.hasHappyFacesOnLoop = false
            }
        } else {
            print("Don`t take, they are angry")
            self.hasHappyFacesOnLoop = false
        }
        
        DispatchQueue.main.async {
            var string = ""
            for (_, value) in self.facesAndEmotions {
                string.append(value.emoticon)
                string.append(" ")
            }
            
            self.emoticonLabel.text = string
        }
    }
}

extension ViewController {
    
    private func classifyImage(_ faceImage: CGImage) {
        self.emotionClassifier.classify(cgImage: faceImage, completion: { emotion in
            self.facesAndEmotions[faceImage] = emotion
            switch emotion {
            case .happy:
                return
            default:
                self.hasHappyFacesOnLoop = false
            }
        })
    }
    
}

extension ViewController {
    
    private func takePicture(_ cgImage: CGImage) {
        self.isTakingPicture = true
        
        self.pictures.append(UIImage(cgImage: cgImage))
        
        self.presentFlashWithFinishAction {
            self.isTakingPicture = false
        }
    }
    
    private func presentFlashWithFinishAction(_ finishAction: @escaping () -> ()) {
        let flashView = UIView()
        flashView.backgroundColor = UIColor.white.withAlphaComponent(1.0)
        flashView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(flashView)
        
        flashView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        flashView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        flashView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        flashView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        UIView.animate(withDuration: 0.6, animations: {
            flashView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        }) { (value) in
            flashView.removeFromSuperview()
            finishAction()
        }
    }
    
    @objc func didTapDoneButton() {
        let photosCollectionViewController = PhotosCollectionViewController(photos: self.pictures)
        self.navigationController?.pushViewController(photosCollectionViewController, animated: true)
    }
    
}

extension ViewController {
    
    private func snapOutputHandler(ciImage: CIImage?) {
        if !self.isTakingPicture {
            guard let ciImage = ciImage else { return }
            self.faceDetector?.detectFaceOn(ciImage)
        }
    }
    
}

